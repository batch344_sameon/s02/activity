import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int[] first_five_prime_numbers = new int[5];
        first_five_prime_numbers[0] = 2;
        first_five_prime_numbers[1] = 3;
        first_five_prime_numbers[2] = 5;
        first_five_prime_numbers[3] = 7;
        first_five_prime_numbers[4] = 11;
        System.out.println("The first prime number is: " + first_five_prime_numbers[0]);
        System.out.println("The second prime number is: " + first_five_prime_numbers[1]);
        System.out.println("The third prime number is: " + first_five_prime_numbers[2]);
        System.out.println("The fourth prime number is: " + first_five_prime_numbers[3]);
        System.out.println("The fifth prime number is: " + first_five_prime_numbers[4]);

        ArrayList<String> friends = new ArrayList<String>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");
        System.out.println("\nMy friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>(){
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };
        System.out.println("\nOur current inventory consists of: " + inventory);
    }
}