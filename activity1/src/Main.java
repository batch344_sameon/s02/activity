import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner year_scanner = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year.");
        int year_value = year_scanner.nextInt();
        if(year_value % 4 == 0){
            System.out.println(year_value + " is a leap year");
        }
        else{
            System.out.println(year_value + " is NOT a leap year");
        }
    }
}